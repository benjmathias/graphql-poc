# Conf and environment variables

## General :
The conf is managed through a python function : 
- maya-api\src\app\helpers\conf_handler.py

It retrieves the config static vars and convert them to a python dict.  
The python conf dict is as follow : 
```
"<SECTION>_<VAR_KEY>=<VAR_VALUE>" except for "DEFAULT" section where <SECTION>_ part is skipped.  
```
If value is empty in the source file (config.ini), it will try to get the value from the environment variable.  

## Dev Usage : 
The config file is stored in "src/config.ini".  
Just insert the values you need and restart the app.  

## Deployment process :
Some env vars will not often change, these are left in the config.ini file.  
For the rest, there have to be filled in Gitlab Web Interface.  
They are passed through the manifest.yaml to the CFY environment.  
