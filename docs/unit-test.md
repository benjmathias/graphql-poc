# Unit Test

There is a job in the pipeline that run the unit tests specifically\
You need to run the pipeline through the web interface to trigger this job

To run the unit test locally, you need :
- python3.7 (minimum)
- python3-pip
- pipenv
- pytest
- all the modules required for this project (listed in requirements.txt + Pipfile)

How to install the prerequisites :
````bash
# cd to the root of this project
sudo apt update
sudo apt install python3.7
sudo apt install python3-pip
python3.7 -m pip install pipenv
python3.7 -m pipenv lock --requirements >> requirements.txt
python3.7 -m pip install -r requirements.txt
````

How to run the unit tests :
````bash
# cd to the root of this project
python3.7 -m pytest src/tests/unit
````