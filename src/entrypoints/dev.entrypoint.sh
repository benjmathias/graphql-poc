#!/bin/bash

PORT=${PORT:-443}

chmod +x /wait-for-it.sh
bash /wait-for-it.sh
pipenv run uvicorn app.main:app --reload --host 0.0.0.0 --workers 4 --port "${PORT}"
