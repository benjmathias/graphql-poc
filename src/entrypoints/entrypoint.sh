#!/bin/bash

PORT=${PORT:-443}

chmod +x /wait-for-it.sh
bash /wait-for-it.sh dwh-mock-host:3306 -t 120
pipenv run uvicorn app.main:app --host 0.0.0.0 --workers 4 --port "${PORT}"
