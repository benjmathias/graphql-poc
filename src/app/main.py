import os
import uvicorn
from fastapi import FastAPI
from .helpers.errors.override_default import override_default
from .routers import status_route, graphql_route
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
override_default(app)

origins = [
    "http://localhost",
    "http://localhost:8888",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

v1 = "/api/v1"
app.include_router(status_route.router, prefix=v1)
app.include_router(graphql_route.router, prefix=v1)


def main():
    port = os.getenv('PORT', '443')
    uvicorn.run(app, host="0.0.0.0", port=int(port))


if __name__ == "__main__":
    main()
