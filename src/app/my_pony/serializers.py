from graphene_pydantic import PydanticInputObjectType, PydanticObjectType
from pydantic import BaseModel


class HostModel(BaseModel):
    host_id: int
    host_nom: str
    host_principal: str = None
    host_additional_info: str = None
    host_mach_id: int = None
    host_os_id: int = None


class HostGrapheneModel(PydanticObjectType):
    """
    Custom serializer for DWH Host data
    """
    class Meta:
        model = HostModel


class HostGrapheneInputModel(PydanticInputObjectType):
    class Meta:
        model = HostModel
        exclude_fields = 'host_id'
