from pony.orm import PrimaryKey, Required, Optional


def host_class_generator():
    return {
        '_table_': 'sys_hosts',
        'host_id': PrimaryKey(int, auto=True),
        'host_nom': Required(str),
        'host_principal': Optional(str),
        'host_additional_info': Optional(str),
        'host_mach_id': Optional(int),
        'host_os_id': Optional(int),
    }
