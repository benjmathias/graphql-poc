from ..my_pony.models.dwh.sys_hosts import host_class_generator
from pony import orm as pny

pny.sql_debug(True)
database = pny.Database()
database.bind(
    provider='mysql',
    host='dwh-mock',
    user='graphql',
    passwd='graphql',
    db='hebex_dwh'
)

# We declare pony classes from models
Host = type('Host', (database.Entity,), host_class_generator())

database.generate_mapping(create_tables=False)
