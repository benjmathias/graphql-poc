from graphene import Mutation
from ...my_pony.db import Host
from ...my_pony.serializers import HostGrapheneModel, HostGrapheneInputModel
from pony.orm import db_session, commit


class CreateHost(Mutation):
    class Arguments:
        host_details = HostGrapheneInputModel()

    Output = HostGrapheneModel

    @staticmethod
    @db_session
    def mutate(parent, info, host_details):
        host = Host(**host_details)
        commit()
        return host
