from graphene import List
from ...my_pony.db import Host
from ...my_pony.serializers import HostGrapheneModel
from pony.orm import db_session
from graphene import ObjectType


class ListQuery(ObjectType):
    list_host = List(HostGrapheneModel)

    @staticmethod
    @db_session
    def resolve_list_host(parent, info):
        return [h for h in Host.select()]
