from graphene import ObjectType, List, String, Int
from ...my_pony.db import Host
from ...my_pony.serializers import HostGrapheneModel
from pony.orm import db_session


class GetQuery(ObjectType):
    get_host_name = String(required=True, host_id=Int(required=True))
    get_object = List(HostGrapheneModel, required=True, host_id=Int(required=True))

    @staticmethod
    @db_session
    def resolve_get_host_name(parent, info, host_id):
        return Host[host_id].host_nom

    @staticmethod
    @db_session
    def resolve_get_object(parent, info, host_id):
        return [Host.get(host_id=host_id)]
