"""
The graphQL calls implementation
"""
from .mutation.create import CreateHost
from .query.get import GetQuery
from .query.list import ListQuery


# We create the main Query class from sub classes
class Query(ListQuery, GetQuery):
    pass


class Mutation(CreateHost):
    create_host = CreateHost.Field()
