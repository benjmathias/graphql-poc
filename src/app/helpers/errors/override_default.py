from fastapi.responses import JSONResponse
from fastapi import FastAPI


def override_default(app: FastAPI):
    # Override default Exception 500
    @app.exception_handler(Exception)
    async def http_exception_handler(request, exc):
        res = JSONResponse({
            "detail": {
                'code': 500,
                'message': f"Internal Server Error : {type(exc).__name__} : {exc}"
            }
        }, status_code=500)
        return res
