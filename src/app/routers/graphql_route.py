"""
The graphql route of the API
"""
import graphene
from fastapi import Request, APIRouter
from ..schema.schema import Query, Mutation
from ..playground import GraphQLApp

router = APIRouter()
graphql_app = GraphQLApp(schema=graphene.Schema(query=Query, mutation=Mutation))


@router.api_route("/graphql", methods=["GET", "POST"])
async def graphql(request: Request):
    return await graphql_app.handle_graphql(request=request)
