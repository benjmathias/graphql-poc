"""
The status route of the API
"""
from fastapi import APIRouter

router = APIRouter()


@router.get("/status")
def status_route() -> dict:
    """ route that check fastapi status and components (dependencies) """

    response_components = {"HELLO": "STATUS"}

    return response_components
